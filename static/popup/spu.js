var Spu = function(options) {
    var self = this;
    var settings = {
        height: 300,
        width: 438,
        eleList: null,
        title: "",
        fnBtn: {
            "text": "确定",
            "bgcolor": "#e78170"
        },
        cancelBtn: {
            "text": "取消",
            "bgcolor": "#cccccc"
        },
        message: ""
    };
    options && self.extend(settings, options);
    self.height = settings.height;
    self.width = settings.width;
    self.eleList = settings.eleList;
    self.title = settings.title;
    self.fnBtn = settings.fnBtn;
    self.cancelBtn = settings.cancelBtn;
    self.message = settings.message;
};


Spu.prototype.extend = function(target, source) {
    for (prop in target) {
        if (source.hasOwnProperty(prop)) {
            target[prop] = source[prop];
        }
    }
    return target;
};

Spu.prototype.initBg = function() {
    var self = this;
    var spuBg = document.createElement("DIV");
    spuBg.className = "spu-background";
    return spuBg;
};

Spu.prototype.initCo = function() {
    var self = this;
    var spuCo = document.createElement("DIV");
    // 定义弹窗样式
    spuCo.className = "spu-content";
    spuCo.style.width = self.width + "px";
    // spuCo.style.height = self.height + "px";
    spuCo.style.marginLeft = -(self.width / 2) + "px";
    spuCo.style.marginTop = -(self.height / 2) + "px";

    var content = (self.title ? self.initTitle() : "")
                + (self.eleList ? self.initEleList() : "")
                + (self.message ? self.initMessage() : "")
                + ((self.fnBtn || self.cancelBtn) ? self.initBottom() : "");
    spuCo.innerHTML += content;
    return spuCo;
};

Spu.prototype.initTitle = function() {
    var self = this;
    var spuTitle = document.createElement("DIV"),
        spuTitleContent = document.createElement("P");
    spuTitle.className = "spu-title";
    spuTitleContent.className = "spu-title-content";
    spuTitleContent.innerHTML = self.title;
    spuTitle.appendChild(spuTitleContent);
    return spuTitle.outerHTML;
};

Spu.prototype.initMessage = function () {
    var self = this,
        content = "";
    var spuMessage = document.createElement("DIV");
    spuMessage.className = "spu-message";
    if (self.message) {
        content = self.initMessageContent();
    }
    spuMessage.innerHTML = content;
    return spuMessage.outerHTML;
};

Spu.prototype.initEleList = function() {
    var self = this;
    var eleList = self.eleList;
    var spuEleList = document.createElement("DIV");
    spuEleList.className = "spu-ele-list";
    // 创建里面的内容
    if (eleList) {
        self.createEleList(eleList, spuEleList);
    }
    return spuEleList.outerHTML;
};

Spu.prototype.initBottom = function() {
    var self = this,
        content = "";
    var spuBottom = document.createElement("DIV");
    spuBottom.className = "spu-bottom";
    if (self.fnBtn) {
        content += self.initFnBtn();
    }
    if (self.cancelBtn) {
        content += self.initCancelBtn();
    }
    spuBottom.innerHTML = content;
    return spuBottom.outerHTML;
};

Spu.prototype.initFnBtn = function() {
    var self = this,
        fnBtn = self.fnBtn;
    var spuFnBtn = document.createElement("BOTTON");
    spuFnBtn.className = "spu-fn-btn";
    spuFnBtn.innerHTML = fnBtn.text;
    spuFnBtn.style.backgroundColor = fnBtn.bgcolor;
    return spuFnBtn.outerHTML;
};

Spu.prototype.initCancelBtn = function() {
    var self = this,
        cancelBtn = self.cancelBtn;
    var spuCancelBtn = document.createElement("BOTTON");
    spuCancelBtn.className = "spu-cancel-btn";
    spuCancelBtn.innerHTML = cancelBtn.text;
    spuCancelBtn.style.backgroundColor = cancelBtn.bgcolor;
    return spuCancelBtn.outerHTML;
};

Spu.prototype.createEleList = function(eleList, targetEle) {
    var self = this;
    var arr = []; // 这是用来装创建的html元素的数组
    for (var i = 0, len = eleList.length; i < len; i ++) {
        if (eleList[i].constructor == Object) {
            var tagName = eleList[i].hasOwnProperty("tag") ? eleList[i].tag.toUpperCase() : "DIV";
            arr.push(document.createElement(tagName));
            // 将元素的属性遍历到dom中
            for (prop in eleList[i]) {
                if (prop != "content" && prop != "tag") {
                    arr[i].setAttribute(prop, eleList[i][prop]);
                } else if (prop == "content") {
                    if (eleList[i][prop].constructor == String) {
                        arr[i].innerHTML = eleList[i][prop];
                    } else if (eleList[i][prop].constructor == Array) {
                        self.createEleList(eleList[i][prop], arr[i]);
                    }
                }
            }
        } else {
            arr.push(eleList[i]);
        }
    }
    for (var i = 0, len = arr.length; i < len; i ++) {
        if (arr[i].constructor == String) {
            targetEle.innerHTML += arr[i];
        } else {
            targetEle.innerHTML += arr[i].outerHTML;
        }
    }
};

Spu.prototype.initMessageContent = function() {
    var self = this,
        message = self.message;
    var messageContent = document.createElement("P");
    messageContent.className = "spu-message-content"
    if (message.constructor == String) {
        messageContent.innerHTML = message;
    }
    return messageContent.outerHTML;
};

Spu.prototype.init = function() {
    var spuCo = this.initCo();
    var spuBg = this.initBg();
    var first = document.body.firstChild;
    document.body.insertBefore(spuCo, first);
    document.body.insertBefore(spuBg, spuCo);
};

Spu.prototype.close = function() {
    // var spuCo = this.initCo();
    // var spuBg = this.initBg();
    // console.log(spuCo);
    // console.log(spuBg.style.cssText += "display: none;");
    // spuBg.style.display = "none";
};

Spu.prototype.render = function() {
};

// Spu.prototype.addEvent = function(ele, type, fn) {
//     if (typeof ele == "undefined") {
//         return false;
//     }
//     if (ele.addEventListener) {
//         ele.addEventListener(type, fn, false);
//     } else if (ele.attachEvent) {
//         ele.attachEvent("on" + type, function() {
//             fn.call(ele, window.event);
//         });
//     } else {
//         ele["on" + type] = fn;
//     }
// }
// Spu.prototype.removeEvent = function(ele, type, fn) {
//     if (typeof ele == "undefined") {
//         return false;
//     }
//     if (ele.removeEventLinstener) {
//         ele.removeEventLinstener(type, fn, false);
//     } else if (ele.detachEvent) {
//         ele.detachEvent("on" + type, function() {
//             fn.call(ele, window.event);
//         });
//     } else {
//         ele["on" + type] = null;
//     }
// };